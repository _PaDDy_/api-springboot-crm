package fr.eftl.dev.api.springboot.crm.rest.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.eftl.dev.api.springboot.crm.dto.CustomerDto;
import fr.eftl.dev.api.springboot.crm.mapper.CustomerMapper;
import fr.eftl.dev.api.springboot.crm.model.Customer;
import fr.eftl.dev.api.springboot.crm.service.CustomerService;

@Validated
@RestController
@RequestMapping("/customers")
@CrossOrigin(value = { "*" }, allowedHeaders = { "*" })
public class CustomerRestController {

	@Autowired
	CustomerService customerService;

	@Autowired
	CustomerMapper customerMapper;

	@SuppressWarnings("unchecked")
	@GetMapping("/{id}")
	public ResponseEntity<CustomerDto> getCustomerById(@NotNull @PathVariable(value = "id") Integer id) {
		Customer customer = customerService.getCustomerById(id);

		if (customer == null) {
			return (ResponseEntity<CustomerDto>) ResponseEntity.notFound();
		}

		return ResponseEntity.ok().body(customerMapper.mapCustomerToCustomerDto(customer));
	}

	@SuppressWarnings("unchecked")
	@GetMapping()
	public ResponseEntity<List<CustomerDto>> getAllCustomers() {
		List<Customer> customers;
		customers = customerService.getAllCustomers();

		if (customers.isEmpty()) {
			return (ResponseEntity<List<CustomerDto>>) ResponseEntity.notFound();
		}

		List<CustomerDto> dtoCustomers = new ArrayList<>();
		for (Customer customer : customers) {
			dtoCustomers.add(customerMapper.mapCustomerToCustomerDto(customer));
		}

		return ResponseEntity.ok().body(dtoCustomers);
	}

	@GetMapping("/IsActive")
	public ResponseEntity<List<CustomerDto>> getCustomersByActive(@RequestParam boolean active) {
		List<Customer> customersActive = customerService.getCustomersByActive(active);

		List<CustomerDto> dtoCustomers = new ArrayList<>();
		for (Customer customer : customersActive) {
			dtoCustomers.add(customerMapper.mapCustomerToCustomerDto(customer));
		}
		return ResponseEntity.ok().body(dtoCustomers);
	}

	@GetMapping("/mobile")
	public ResponseEntity<List<CustomerDto>> getCustomerWithMobile() {
		List<Customer> customersWithMobile = customerService.getCustomersWithMobile();

		List<CustomerDto> dtoCustomersWithMobile = new ArrayList<>();
		for (Customer customer : customersWithMobile) {
			dtoCustomersWithMobile.add(customerMapper.mapCustomerToCustomerDto(customer));
		}
		return ResponseEntity.ok().body(dtoCustomersWithMobile);
	}

	@PutMapping("/{id}")
	public ResponseEntity<CustomerDto> updateCustomer(@Valid @RequestBody final CustomerDto customerDto,
			@NotNull @PathVariable Integer id) {
		Customer customerToBeUpdated = customerMapper.mapCustomerDtoToCustomer(customerDto);
		customerToBeUpdated.setId(id);
		Customer updatedCustomer = customerService.updateCustomer(customerToBeUpdated);
		return ResponseEntity.ok().body(customerMapper.mapCustomerToCustomerDto(updatedCustomer));
	}

	@PostMapping()
	public ResponseEntity<CustomerDto> createCustomer(@Valid @RequestBody final CustomerDto customerDto) {
		Customer customer = customerMapper.mapCustomerDtoToCustomer(customerDto);
		Customer newCustomer = customerService.createCustomer(customer);

		return ResponseEntity.created(URI.create("/customers/" + newCustomer.getId()))
				.body(customerMapper.mapCustomerToCustomerDto(newCustomer));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteCustomer(@NotNull @PathVariable(value = "id") Integer id) {
		customerService.deleteCustomer(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
package fr.eftl.dev.api.springboot.crm.mapper;

import org.springframework.stereotype.Component;

import fr.eftl.dev.api.springboot.crm.dto.CustomerDto;
import fr.eftl.dev.api.springboot.crm.model.Customer;

@Component
public class CustomerMapper {

	public CustomerDto mapCustomerToCustomerDto(Customer customer) {
		CustomerDto customerDto = new CustomerDto();
		customerDto.setId(customer.getId());
		customerDto.setFirstname(customer.getFirstname());
		customerDto.setLastname(customer.getLastname());
		customerDto.setCompany(customer.getCompany());
		customerDto.setMail(customer.getMail());
		customerDto.setPhone(customer.getPhone());
		customerDto.setMobile(customer.getMobile());
		customerDto.setNotes(customer.getNotes());
		customerDto.setActive(customer.isActive());
		return customerDto;
	}

	public Customer mapCustomerDtoToCustomer(CustomerDto customerDto) {
		Customer customer = new Customer();
		customer.setId(customerDto.getId());
		customer.setFirstname(customerDto.getFirstname());
		customer.setLastname(customerDto.getLastname());
		customer.setCompany(customerDto.getCompany());
		customer.setMail(customerDto.getMail());
		customer.setPhone(customerDto.getPhone());
		customer.setMobile(customerDto.getMobile());
		customer.setNotes(customerDto.getNotes());
		customer.setActive(customerDto.isActive());
		return customer;
	}

}

package fr.eftl.dev.api.springboot.crm.service;

import java.util.List;

import fr.eftl.dev.api.springboot.crm.model.Customer;

public interface CustomerService {

	/**
	 * Get a customer by id
	 * 
	 * @param id the id
	 * @return the Customer
	 */
	Customer getCustomerById(Integer id);

	/**
	 * Get all customers
	 * 
	 * @return a list of customers
	 */
	List<Customer> getAllCustomers();

	/**
	 * Get customers by active status
	 * 
	 * @param active true or false
	 * @return a list of customers by active status
	 */
	List<Customer> getCustomersByActive(Boolean active);

	/**
	 * Get customers with mobile number
	 * 
	 * @return a list of customers with a mobile number
	 */
	List<Customer> getCustomersWithMobile();

	/**
	 * Create a customer
	 * 
	 * @param customer the customer to create
	 */
	Customer createCustomer(Customer customer);

	/**
	 * Update a customer
	 * 
	 * @param customer the customer to update
	 * @return
	 */
	Customer updateCustomer(Customer customer);

	/**
	 * Delete a customer
	 * 
	 * @param customerId the id of the customer to delete
	 */
	void deleteCustomer(Integer customerId);

	/**
	 * Update the customer's status (active)
	 * 
	 * @param customerId Id of the customer
	 * @param active     boolean value of the status to patch
	 * @throws Exception
	 */
	void patchCustomerStatus(Integer customerId, boolean active) throws Exception;
}
package fr.eftl.dev.api.springboot.crm.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto {
	
    private Integer id;
    
    @NotNull
    @Size(min = 2, max = 30)
    private String username;
    
    @NotNull
    @Size(min = 2, max = 255)
    private String password;
    
    @NotNull
    @Email
    @Size(min = 2, max = 255)
    private String mail;
    
    @NotNull
    @Pattern(regexp = "USER|ADMIN")
    private String grants;

    public UserDto() {
    }

    public UserDto(Integer id, String username, String password, String mail, String grants) {
        super();
        this.id = id;
        this.username = username;
        this.password = password;
        this.mail = mail;
        this.grants = grants;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getGrants() {
        return grants;
    }

    public void setGrants(String grants) {
        this.grants = grants;
    }

}



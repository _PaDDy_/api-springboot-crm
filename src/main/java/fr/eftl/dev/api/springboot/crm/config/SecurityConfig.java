package fr.eftl.dev.api.springboot.crm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	@Bean
	public UserDetailsService userDetailsService() {
		return new MyUserDetailsServiceImpl();
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService());
		authProvider.setPasswordEncoder(passwordEncoder());
		return authProvider;
	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) {
		auth.authenticationProvider(authenticationProvider());

	}

	/**
	 * This section defines the security policy for the app.
	 * 
	 * .antMatchers(HttpMethod.POST, URI_CUSTOMERS).hasRole(ROLE_ADMIN)
	 * 
	 * 
	 * @param http
	 * @throws Exception
	 * @see org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter#configure(org.springframework.security.config.annotation.web.builders.HttpSecurity)
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.httpBasic().and().authorizeRequests()
				.antMatchers("/v2/api-docs", "/swagger-resources/**", "/swagger-ui/**", "/users/login").permitAll()
				.antMatchers("/users/**").hasRole("ADMIN").antMatchers(HttpMethod.GET, "/customers/**")
				.hasAnyRole("USER", "ADMIN").antMatchers(HttpMethod.PUT, "/customers/**").hasAnyRole("USER", "ADMIN")
				.antMatchers(HttpMethod.POST, "/customers/**").hasAnyRole("USER", "ADMIN")
				.antMatchers(HttpMethod.PATCH, "/customers/**").hasAnyRole("USER", "ADMIN")
				.antMatchers(HttpMethod.DELETE, "/customers/**").hasRole("ADMIN")
				.antMatchers(HttpMethod.GET, "/orders/**").hasAnyRole("USER", "ADMIN")
				.antMatchers(HttpMethod.PUT, "/orders/**").hasAnyRole("USER", "ADMIN")
				.antMatchers(HttpMethod.POST, "/orders/**").hasAnyRole("USER", "ADMIN")
				.antMatchers(HttpMethod.PATCH, "/orders/**").hasAnyRole("USER", "ADMIN")
				.antMatchers(HttpMethod.DELETE, "/orders/**").hasRole("ADMIN").anyRequest().authenticated();

		http.csrf().disable();
	}

}

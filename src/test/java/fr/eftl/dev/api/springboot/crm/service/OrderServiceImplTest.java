package fr.eftl.dev.api.springboot.crm.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.eftl.dev.api.springboot.crm.app.exception.UnknownResourceException;
import fr.eftl.dev.api.springboot.crm.model.Customer;
import fr.eftl.dev.api.springboot.crm.model.Order;
import fr.eftl.dev.api.springboot.crm.repository.OrderRepository;
import fr.eftl.dev.api.springboot.crm.service.impl.OrderServiceImpl;

@ExtendWith(MockitoExtension.class)
public class OrderServiceImplTest {

	@InjectMocks
	private OrderServiceImpl orderService;

	@Mock
	private OrderRepository orderRepository;

	@BeforeEach
	public void init() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void testGetAllOrders() {
		List<Order> orders = new ArrayList<>();
		Customer customerTest = new Customer();
		customerTest.setId(3);
		orders.add(new Order(3, "formation test", 333.3, 3.3, 33.3, "status test", "type test", "notes test",
				customerTest));
		orders.add(new Order(3, "formation test2", 222.2, 2.2, 22.2, "status test2", "type test2", "notes test2",
				customerTest));
		Mockito.when(orderRepository.findAll()).thenReturn(orders);
		List<Order> ordersGot = orderService.getAllOrders();
		assertEquals(2, ordersGot.size());
	}

	@Test
	void testGetOrderById_OK() {
		Customer customerTest = new Customer();
		customerTest.setId(3);
		Optional<Order> orderTest = Optional.of(new Order(3, "formation test", 333.3, 3.3, 33.3, "status test",
				"type test", "notes test", customerTest));
		Mockito.when(orderRepository.findById(3)).thenReturn(orderTest);
		Order order = orderService.getOrderById(3);
		assertEquals(3, order.getId());
	}

	@Test
	void testGetOrderById_KO() {
		Mockito.when(orderRepository.findById(3)).thenThrow(UnknownResourceException.class);
		assertThrows(UnknownResourceException.class, () -> orderService.getOrderById(3));
	}

	@Test
	void testCreateOrder() {
		Order order = new Order();
		order.setLabel("Test Label");
		order.setAdrEt(333.33);
		order.setTva(33.33);

		Mockito.when(orderRepository.save(order)).thenReturn(order);
		Order createdOrder = orderService.createOrder(order);
		assertEquals("Test Label", createdOrder.getLabel());
	}

	@Test
	void testUpdateOrder_ok() {
		Order order = new Order();
		order.setId(3);
		order.setNotes("notes test - updateOrder");

		Optional<Order> orderTest = Optional.of(order);
		Mockito.when(orderRepository.findById(3)).thenReturn(orderTest);
		Mockito.when(orderRepository.save(order)).thenReturn(order);
		try {
			orderService.updateOrder(order);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	void testUpdateOrder_KO() {
		Order order = new Order();
		order.setId(4);
		order.setType("test type");
		Optional<Order> orderTest = Optional.empty();

		Mockito.when(orderRepository.findById(4)).thenReturn(orderTest);
		assertThrows(UnknownResourceException.class, () -> orderService.updateOrder(order),
				"UnknownResourceException attendue !");
	}

	@Test
	void testDeleteOrder() {
		Order order = new Order();
		order.setId(90);
		Optional<Order> orderTestDelete = Optional.of(order);

		Mockito.when(orderRepository.findById(90)).thenReturn(orderTestDelete);
		Mockito.doNothing().when(orderRepository).delete(order);

		try {
			orderService.deleteOrder(90);
		} catch (Exception e) {
			fail();
		}
		Mockito.verify(orderRepository, Mockito.times(1)).delete(order);
	}

	@Test
	void testDeleteOrder_KO() {
		Order order = new Order();
		order.setId(90);
		Optional<Order> orderTest = Optional.empty();

		Mockito.when(orderRepository.findById(90)).thenReturn(orderTest);
		assertThrows(UnknownResourceException.class, () -> orderService.deleteOrder(90),
				"UnknownResourceException attendue !");
		Mockito.verify(orderRepository, Mockito.times(0)).delete(order);
	}

	@Test
	void testPatchOrder_ok() {
		Order order = new Order();
		order.setId(3);
		order.setStatus("status test - PatchOrder");

		Optional<Order> orderTest = Optional.of(order);
		Mockito.when(orderRepository.findById(3)).thenReturn(orderTest);
		Mockito.when(orderRepository.save(order)).thenReturn(order);
		try {
			orderService.patchOrderStatus(order.getId(), order.getStatus());
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	void testPatchOrder_KO() {
		Order order = new Order();
		order.setId(4);
		order.setStatus("status test - PatchOrder_KO");
		Optional<Order> orderTest = Optional.empty();

		Mockito.when(orderRepository.findById(4)).thenReturn(orderTest);
		assertThrows(UnknownResourceException.class,
				() -> orderService.patchOrderStatus(order.getId(), order.getStatus()),
				"UnknownResourceException attendue !");
	}

	@Test
	void testGetOrderByTypeAndStatus_OK() {
		Customer customerTest = new Customer();
		customerTest.setId(4);
		Order orderTest = new Order(3, "formation test", 333.3, 3.3, 33.3, "status test", "type test", "notes test",
				customerTest);
		List<Order> orders = new ArrayList<>();
		orders.add(orderTest);
		Optional<List<Order>> OrderOp = Optional.of(orders);
		Mockito.when(orderRepository.findByTypeAndStatus("type test", "status test")).thenReturn(OrderOp);
		List<Order> ordersGot = orderService.getOrderByTypeAndStatus(orderTest.getType(), orderTest.getStatus());
		assertEquals(1, ordersGot.size());
	}

	@Test
	void testGetOrderByStatus_OK() {
		Customer customerTest = new Customer();
		customerTest.setId(4);
		Order orderTest = new Order(3, "formation test", 333.3, 3.3, 33.3, "status test", "type test", "notes test",
				customerTest);
		List<Order> orders = new ArrayList<>();
		orders.add(orderTest);
		Optional<List<Order>> OrderOp = Optional.of(orders);
		Mockito.when(orderRepository.findByStatus("status test")).thenReturn(OrderOp);
		List<Order> ordersGot = orderService.getOrderByStatus(orderTest.getStatus());
		assertEquals(1, ordersGot.size());
	}

	@Test
	void testGetOrderByStatus_KO() {
		Mockito.when(orderRepository.findByStatus("status tests")).thenThrow(UnknownResourceException.class);
		assertThrows(UnknownResourceException.class, () -> orderService.getOrderByStatus("status tests"));
	}

	@Test
	void testGetOrderByCustomer_OK() throws Exception {
		Customer customerTest = new Customer();
		customerTest.setId(4);
		Order orderTest = new Order(3, "formation test", 333.3, 3.3, 33.3, "status test", "type test", "notes test",
				customerTest);
		List<Order> orders = new ArrayList<>();
		orders.add(orderTest);
		Optional<List<Order>> OrderOp = Optional.of(orders);
		Mockito.when(orderRepository.findByCustomer(customerTest)).thenReturn(OrderOp);
		List<Order> ordersGot = orderService.getOrderByCustomer(customerTest);
		assertEquals(1, ordersGot.size());
	}

	@Test
	void testGetOrderByCustomer_KO() {
		Customer customerTest = new Customer();
		customerTest.setId(4);
		Mockito.when(orderRepository.findByCustomer(customerTest)).thenThrow(UnknownResourceException.class);
		assertThrows(UnknownResourceException.class, () -> orderService.getOrderByCustomer(customerTest));
	}

}